/*
 * Copyright e.blindauer (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;


import net.agat.adeunistra.BuildConfig;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.util.Log;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpGet;
import ch.boye.httpclientandroidlib.impl.client.HttpClients;
import ch.boye.httpclientandroidlib.util.EntityUtils;


/**
 * Created by e.blindauer on 18/07/14.
 */
public class AdeunistraServerAccessor {
    private final String TAG = this.getClass().getSimpleName();

    private class Seances implements Serializable {
        ArrayList<Seance> results;
    }

    public ArrayList<Seance> getSeances(Integer projectId, ArrayList<Integer> resId) throws Exception {
        Log.d(TAG,"AdeunistraServerAccessor/getSeances all resId");
        ArrayList <Seance> seances = new ArrayList<Seance>();
        for (Integer s : resId) {
            ArrayList <Seance> newseances ;
            newseances = getSeances(projectId,s);
            seances.addAll(newseances);

        }
        return seances;
    }


    public ArrayList<Seance> getSeances(Integer projectId,  Integer resId) throws Exception {
        Log.d(TAG, "AdeunistraServerAccessor/getSeances auth[" + projectId + resId + "]");

        HttpClient httpClient = HttpClients.custom()
                .setUserAgent(BuildConfig.WEBSIGNATURE)
                .build();


        String url = BuildConfig.ADEPROXY + projectId+ "/" + resId;
        ArrayList <Seance> seances = new ArrayList<Seance>();

        HttpGet httpGet = new HttpGet(url);

        try {
            HttpResponse response = httpClient.execute(httpGet);

            String responseString = EntityUtils.toString(response.getEntity(),"UTF-8");
            //Log.d(TAG, "> Response= " + responseString);

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Erreur récuperation seances");
            }

            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                InputSource is = new InputSource(new StringReader(responseString));
                Document xmlDocument = builder.parse(is);

                NodeList eventsNodes = xmlDocument.getElementsByTagName("event");
                for(int i=0; i<eventsNodes.getLength(); i++){
                    String name, date, dated, datef, datem, salle = "", prof = "", grp = "";
                    int eventid;

                    // Verifier ici que les attributs sont presents et lever uen exception si besoin
                    if ( (eventsNodes.item(i).getAttributes().getNamedItem("name") == null)
                            || (eventsNodes.item(i).getAttributes().getNamedItem("startHour") == null)
                            || (eventsNodes.item(i).getAttributes().getNamedItem("endHour") == null)
                            || (eventsNodes.item(i).getAttributes().getNamedItem("date") == null)
                            ) {
                        Log.d(TAG, "Event " + (i+1) + "Missing attribute");
                        throw new Exception("Erreur sur contenu xml");
                    }

                    name = eventsNodes.item(i).getAttributes().getNamedItem("name").getTextContent();
                    dated = eventsNodes.item(i).getAttributes().getNamedItem("startHour").getTextContent();
                    datef = eventsNodes.item(i).getAttributes().getNamedItem("endHour").getTextContent();
                    date = eventsNodes.item(i).getAttributes().getNamedItem("date").getTextContent();
                    datem = eventsNodes.item(i).getAttributes().getNamedItem("lastUpdate").getTextContent();
                    eventid = Integer.parseInt(
                            eventsNodes.item(i).getAttributes().getNamedItem("id").getTextContent());

                    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                    Date dateCorrect = df.parse(date);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String dateCorrectString = formatter.format(dateCorrect);

                    dated = dateCorrectString + " " + dated;
                    datef = dateCorrectString + " " + datef;


                    NodeList nodeRessources = eventsNodes.item(i).getChildNodes();
                    for(int j=0; j<nodeRessources.getLength(); j++) {
                        if (nodeRessources.item(j).getNodeName().equals("resources")) {
                            NodeList nodeRessource = nodeRessources.item(j).getChildNodes();
                            for(int k=0; k<nodeRessource.getLength(); k++) {
                                Node node = nodeRessource.item(k);
                                if (node.getNodeName().equals("resource")) {
                                    if (node.getAttributes().getNamedItem("category").getNodeValue().equals("instructor")) {
                                        if (prof != "")
                                            prof = prof + ", " + node.getAttributes().getNamedItem("name").getNodeValue();
                                        else
                                            prof = node.getAttributes().getNamedItem("name").getNodeValue();
                                    };
                                    if (node.getAttributes().getNamedItem("category").getNodeValue().equals("classroom")) {
                                        if (salle != "")
                                            salle = salle + ", " + node.getAttributes().getNamedItem("name").getNodeValue();
                                        else
                                            salle = node.getAttributes().getNamedItem("name").getNodeValue();
                                    };
                                    if (node.getAttributes().getNamedItem("category").getNodeValue().equals("trainee")) {
                                        if (grp != "")
                                            grp = grp + ", " + node.getAttributes().getNamedItem("name").getNodeValue();
                                        else
                                            grp = node.getAttributes().getNamedItem("name").getNodeValue();
                                    };

                                }
                            }
                        }
                    }

                    Seance s = new Seance(name, dated, datef, datem, eventid, salle, prof, grp);
                    seances.add(s);

                }
                Log.d(TAG, "NB Event network " + seances.size());

                return seances;

            } catch(Exception ex){
                Log.e(TAG, " msg: " + ex.getMessage());
                throw new Exception("Erreur sur l'analyse du contenu XML du serveur");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("Erreur réseau ?");
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Erreur sur l'analyse du contenu XML du serveur");
        }
    }

}
