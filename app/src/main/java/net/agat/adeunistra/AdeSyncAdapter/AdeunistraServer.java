/*
 * Copyright e.blindauer (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;


import android.util.Log;

import net.agat.adeunistra.BuildConfig;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.boye.httpclientandroidlib.HttpEntity;
import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpGet;
import ch.boye.httpclientandroidlib.impl.client.DefaultRedirectStrategy;
import ch.boye.httpclientandroidlib.impl.client.HttpClientBuilder;
import ch.boye.httpclientandroidlib.util.EntityUtils;
import fr.ece.android.casclient.CasClient;


/**
 * Created by e.blindauer on 17/07/14.
 */
public class AdeunistraServer {

    private final String TAG = this.getClass().getSimpleName();
    String USERAGENT = BuildConfig.WEBSIGNATURE;

    public AdeunistraResponse userSignIn(String user, String pass, String authType) throws Exception {

        Log.d(TAG, "userSignIn");

        AdeunistraResponse r = new AdeunistraResponse();
        r.resId = null ; r.projectId = -1;
        r = getRessourcesADE(user, pass);
/*        r.resId = new ArrayList<Integer>(0);
        r.resId.add(7641);r.resId.add(7258); r.projectId=5;
        */
        return r;
    }
    public AdeunistraResponse getRessourcesADE(String user, String pass) throws Exception {
        Log.d(TAG, "getRessourcesADE");

        AdeunistraResponse r = new AdeunistraResponse();
        HttpClientBuilder preHttpClient = HttpClientBuilder.create();
        preHttpClient.setRedirectStrategy(new DefaultRedirectStrategy());
        preHttpClient.setUserAgent(USERAGENT);
        HttpClient httpClient = preHttpClient.build();

        // on se logge sur CAS pour verification
        CasClient casClient = new CasClient(httpClient, BuildConfig.CASSERVER );
        casClient.login (BuildConfig.ADESERVER, user, pass);

        // on appelle ensuite le WS avec le login
        HttpGet getRequest = new HttpGet(BuildConfig.ADESERVER + user);
        getRequest.addHeader("accept","application/json");
        getRequest.addHeader("Authorization","Token "+ BuildConfig.ADEWSTOKEN);
        HttpResponse response = httpClient.execute(getRequest);

        int statusCode = response.getStatusLine().getStatusCode();
        HttpEntity entity = response.getEntity();
        StringBuilder sb = new StringBuilder();
        if (entity != null) {
            InputStream inputStream = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
            String line;
            while ((line = reader.readLine()) != null) sb.append(line + "\n");
        }
        EntityUtils.consume(response.getEntity());

        // La reponse est de la forme = [{"resources":"7317,7641","username":"e.blindauer","id":"72619"}]
        JSONArray ja = new JSONArray(sb.toString());
        if (ja.length()<1) {
            throw new Exception("WSADE: Pas de personnalisation d'effectuée sur l'ENT!");
        }
        try {
            String[] s = ja.getJSONObject(0).getString("resources").split(",");

            r.resId = new ArrayList<Integer>();
            for (String i : s) r.resId.add(Integer.parseInt(i));
        } catch (Exception e) {
            throw new Exception("WSADE: Personnalisation vide dans l'ENT");
        }

        // Le nouveau WS n'envoie pas le projet courant. l'appli sera  mise à jour chaque année
        r.projectId = BuildConfig.ADEPROJECT;
        if (r.resId.size() == 0) throw new Exception("Problème: la personnalisation est vide!");

        /* DEBUG manuel: */
/*        r.resId = new ArrayList<Integer>(0);
        r.resId.add(8056);
        r.resId.add(28957);
        //r.resId.add(28434);
        r.projectId=1;
    /*   Fin DEBUG manuel  */

        return r;
    }

    public static class AdeunistraResponse implements Serializable {
        public List<Integer> resId;
        public int projectId;
    }
}
