/*
 * Copyright e.blindauer (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
/*
import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;
import com.google.android.vending.licensing.ServerManagedPolicy;
*/
import net.agat.adeunistra.BuildConfig;
import net.agat.adeunistra.R;

import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.KEY_ADE_PROJ;
import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.KEY_ADE_RES;
import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.sServerAuthenticate;
import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.sServerAuthenticate;

/**
 * Created by e.blindauer on 17/07/14.
 */
public class actAuthenticator extends AccountAuthenticatorActivity {


    public final static String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
    public final static String ARG_AUTH_TYPE = "AUTH_TYPE";
    public final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
    public final static String PARAM_USER_PASS = "USER_PASS";
    public final static String ARG_IS_ADDING_NEW_ACCOUNT = "IS_ADDING_ACCOUNT";

    public final static String KEY_ERROR_MESSAGE = "ERR_MSG";


    private final String TAG = this.getClass().getSimpleName();

    private AccountManager mAccountManager;
    private String mAuthTokenType;

    /* la partie licence */
    private static final String BASE64_PUBLIC_KEY = BuildConfig.BASE64_PUBLIC_KEY;
    private static final byte[] SALT = new byte[] {
            -6, 95, 39, -18, -113, -37, 84, 64, 52, 11, -5,
            -5, 57, -17, -76, -93, -111, 111, 64, 87
    };
    private final static String sLICENCED_YES = "license ok";
    private final static String sLICENCED_NO = "Pas de license";
    private final static String sLICENCED_PROBLEM = "Erreur interne de licence";
//    private LicenseCheckerCallback mLicenseCheckerCallback;
//    private LicenseChecker mChecker;
    private Handler mHandler;

    String allowed = "";


    /* Fin la partie licence */



    public actAuthenticator() {

        super();
        Log.d("adeunistra", TAG + "> construct");
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d( TAG, "> on create");
        setContentView(R.layout.activity_auth);
        mAccountManager = AccountManager.get(getBaseContext());

        // license check
/*
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        // Library calls this when it's done.
        mLicenseCheckerCallback = new MyLicenseCheckerCallback();
        // Construct the LicenseChecker with a policy.
        mChecker = new LicenseChecker(
                this, new ServerManagedPolicy(this,
                new AESObfuscator(SALT, getPackageName(), deviceId)),
                BASE64_PUBLIC_KEY);
        mHandler = new Handler();
        doCheck();

        // License check end
*/


        String accountName = getIntent().getStringExtra(ARG_ACCOUNT_NAME);
        mAuthTokenType = getIntent().getStringExtra(ARG_AUTH_TYPE);
        if (mAuthTokenType == null)
            mAuthTokenType = AccountGeneral.AUTHTOKEN_TYPE;
        Log.d( TAG, "> on create mAuthTokenType:"+mAuthTokenType);

        if (accountName != null) {
            ((TextView) findViewById(R.id.accountName)).setText(accountName);
        }

        findViewById(R.id.submit).setEnabled(true);

        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });

    }

/*
    private void doCheck() {
        mChecker.checkAccess(mLicenseCheckerCallback);
    }
*/
    /*
    private class MyLicenseCheckerCallback implements LicenseCheckerCallback {

        public void allow(int policyReason) {
            Log.d(TAG,"License: ok");
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }
            // Should allow user access.
            allowed = sLICENCED_YES;
            displayResult(policyReason);
        }
        public void dontAllow(int policyReason) {
            Log.d(TAG,"License: dontAllow: " + policyReason);
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }

            // Should not allow access. In most cases, the app should assume
            // the user has access unless it encounters this. If it does,
            // the app should inform the user of their unlicensed ways
            // and then either shut down the app or limit the user to a
            // restricted set of features.
            //Button myButton = (Button) findViewById(R.id.submit);

            if (policyReason == Policy.RETRY) {
                Log.d(TAG, "License: dontAllow: Retry");
                displayResult(policyReason);
                return;
            }
            Log.d(TAG, "License: dontAllow: Now allowed");
            allowed = sLICENCED_NO;
            displayResult(policyReason);
        }

        public void applicationError(int errorCode) {
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }
            // This is a polite way of saying the developer made a mistake
            allowed = sLICENCED_PROBLEM;
            displayResult(errorCode);
        }
    }
*/
    private void displayResult(final int result) {
        mHandler.post(new Runnable() {
            public void run() {
                if (allowed == sLICENCED_YES) {
                    ((Button) findViewById(R.id.submit)).setEnabled(true);

                } else {
                    ((Button) findViewById(R.id.submit)).setEnabled(false);
                    AlertDialog.Builder endof = new AlertDialog.Builder(actAuthenticator.this);
                    endof.setMessage("Problème\nCe programme est soumis à licence, mais google n'a pas pu confirmer l'achat de ce logiciel par vous-même. Veuillez retenter.");
                    endof.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
                }

            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG , "> Activity result");

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
  //      mChecker.onDestroy();
    }


    public void submit() {

        final String userName = ((TextView) findViewById(R.id.accountName)).getText().toString();
        final String userPass = ((TextView) findViewById(R.id.accountPassword)).getText().toString();

        final String accountType = getIntent().getStringExtra(ARG_ACCOUNT_TYPE);

        new AsyncTask<String, Void, Intent>() {

            @Override
            protected Intent doInBackground(String... params) {

                Log.d(TAG, "> Started authenticating");

                Bundle data = new Bundle();
                try {

                    AdeunistraServer.AdeunistraResponse resp = sServerAuthenticate.userSignIn(userName, userPass, mAuthTokenType);

                    data.putString(AccountManager.KEY_ACCOUNT_NAME, userName);
                    data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                    data.putString(AccountManager.KEY_AUTHTOKEN, "adeunistra");

                    // We keep the user's object id as an extra data on the account.
                    // It's used later for determine ACL for the data we send to the Parse.com service
                    Bundle userData = new Bundle();
                    String d = TextUtils.join(",",resp.resId);
                    userData.putString(KEY_ADE_RES, d);
                    userData.putString(KEY_ADE_PROJ, ((Integer) resp.projectId).toString());
                    data.putBundle(AccountManager.KEY_USERDATA, userData);
                    data.putString(PARAM_USER_PASS, userPass);

                } catch (Exception e) {
                    data.putString(KEY_ERROR_MESSAGE, e.getMessage());
                }

                final Intent res = new Intent();
                res.putExtras(data);
                return res;
            }

            @Override
            protected void onPostExecute(Intent intent) {
                if (intent.hasExtra(KEY_ERROR_MESSAGE)) {
                    Toast.makeText(getBaseContext(), intent.getStringExtra(KEY_ERROR_MESSAGE), Toast.LENGTH_SHORT).show();
                } else {
                    finishLogin(intent);
                }
            }
        }.execute();
    }

    private void finishLogin(Intent intent) {
        Log.d(TAG, "> finishLogin");

        String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        String accountPassword = intent.getStringExtra(PARAM_USER_PASS);
        final Account account = new Account(accountName, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));

        if (getIntent().getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT, false)) {
            Log.d(TAG, "> finishLogin > addAccountExplicitly");
            String authtoken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
            String authtokenType = mAuthTokenType;
            Log.d( TAG, "> finish login authtokentype"+ authtokenType+" authtoken"+authtoken);

            // Creating the account on the device and setting the auth token we got
            // (Not setting the auth token will cause another call to the server to authenticate the user)
            mAccountManager.addAccountExplicitly(account, accountPassword, intent.getBundleExtra(AccountManager.KEY_USERDATA));
            mAccountManager.setAuthToken(account, authtokenType, authtoken);
        } else {
            Log.d(TAG, "> finishLogin > setPassword");
            mAccountManager.setPassword(account, accountPassword);
        }

        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);
        finish();
    }

}
