/*
 * Copyright e.blindauer (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by e.blindauer on 18/07/14.
 */
public class Seance implements Serializable {

    // Fields
    public String name;
    public String dated;
    public String datef;
    public String datem;
    public int eventid;
    public String salle;
    public String prof;
    public String grp;

    public Seance(String name, String dated, String datef, String datem, int eventid, String salle, String prof, String grp) {
        this.name = name;
        this.dated = dated;
        this.datef = datef;
        this.datem = datem;
        this.eventid = eventid;
        this.salle = salle;
        this.prof = prof;
        this.grp = grp;
    }

    /**
     * Convenient method to get the objects data members in ContentValues object.
     * This will be useful for Content Provider operations,
     * which use ContentValues object to represent the data.
     *
     * @return
     */
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(SeancesHelper.SEANCES_COL_NAME, name);
        values.put(SeancesHelper.SEANCES_COL_DATED, dated);
        values.put(SeancesHelper.SEANCES_COL_DATEF, datef);
        values.put(SeancesHelper.SEANCES_COL_DATEM, datem);
        values.put(SeancesHelper.SEANCES_COL_EVENTID, eventid);
        values.put(SeancesHelper.SEANCES_COL_SALLE, salle);
        values.put(SeancesHelper.SEANCES_COL_PROF, prof);
        values.put(SeancesHelper.SEANCES_COL_GRP, grp);

        return values;
    }

    // Create a Seance object from a cursor
    public static Seance fromCursor(Cursor curSeances) {
        String name = curSeances.getString(curSeances.getColumnIndex(SeancesHelper.SEANCES_COL_NAME));
        String dated = curSeances.getString(curSeances.getColumnIndex(SeancesHelper.SEANCES_COL_DATED));
        String datef = curSeances.getString(curSeances.getColumnIndex(SeancesHelper.SEANCES_COL_DATEF));
        String datem = curSeances.getString(curSeances.getColumnIndex(SeancesHelper.SEANCES_COL_DATEM));
        int eventid = curSeances.getInt(curSeances.getColumnIndex(SeancesHelper.SEANCES_COL_EVENTID));
        String salle = curSeances.getString(curSeances.getColumnIndex(SeancesHelper.SEANCES_COL_SALLE));
        String prof = curSeances.getString(curSeances.getColumnIndex(SeancesHelper.SEANCES_COL_PROF));
        String grp = curSeances.getString(curSeances.getColumnIndex(SeancesHelper.SEANCES_COL_GRP));

        return new Seance(name, dated, datef, datem, eventid, salle, prof, grp);
    }

    // En theorie, on peut utiliser eventid qui devrait etre unique.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if ((o == null) || (this.getClass() != o.getClass())) return false;
        Seance seance = (Seance) o;

        if (this.eventid == seance.eventid) return true;
        return false;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        return result;
    }

    @Override
    public String toString() {
//TODO : calculer correctement date et heured et heuref ca peut aller direcement dans le listview ?
        String date = "";
        String heured = "";
        String heuref = "";
        String salleS;
        String profS;

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date dateCorrect;
        try {
            dateCorrect = df.parse(dated);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            date = formatter.format(dateCorrect);
            formatter = new SimpleDateFormat("HH:mm");
            heured = formatter.format(dateCorrect);
            dateCorrect = df.parse(datef);
            formatter = new SimpleDateFormat("HH:mm");
            heuref = formatter.format(dateCorrect);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (salle.equals(""))
            salleS = "";
        else
            salleS = "("+ salle +")";
        if (prof.equals(""))
            profS = "";
        else
            profS = "("+ prof +")";

        return date + " " + heured + " - " + heuref + " " + salleS + "\n" + name + " " + profS ;
    }
}
