/*
 * Copyright e.blindauer (c) 2016.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import net.agat.adeunistra.R;

/**
 * Created by e.blindauer on 06/09/16.
 */
public class NotificationHelper {
    private static final String TAG = "NotificationHelper";
    public static void signalSyncErrors(Context context, String title, String text) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setContentTitle(title)
                        .setSmallIcon(R.drawable.adeunistrasmall)
                        .setContentText(text);
        Log.d(TAG, "Helper");
        NotificationManager mNotificationManager =
            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

    // mId allows you to update the notification later on.
    int mId = 0;
    mNotificationManager.notify(mId, mBuilder.build());

    }
}

