/*
 * Copyright e.blindauer (c) 2014. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * Created by e.blindauer on 16/07/14.
 */

public class SeancesHelper extends SQLiteOpenHelper {

        private static final String DATABASE_NAME = "adeunistraseances.db";
        private static final int DATABASE_VERSION = 3;

        // DB Table consts
        public static final String SEANCES_TABLE_NAME = "seances";
        public static final String SEANCES_COL_ID = "_id";
        public static final String SEANCES_COL_NAME = "name";
        public static final String SEANCES_COL_DATED = "dated";
        public static final String SEANCES_COL_DATEF = "datef";
        public static final String SEANCES_COL_DATEM = "datem";
        public static final String SEANCES_COL_EVENTID = "eventid";
        public static final String SEANCES_COL_SALLE = "salle";
        public static final String SEANCES_COL_PROF = "prof";
        public static final String SEANCES_COL_GRP = "grp";


        // Database creation sql statement
        public static final String DATABASE_CREATE = "create table "
                + SEANCES_TABLE_NAME + "(" +
                SEANCES_COL_ID + " integer primary key autoincrement, " +
                SEANCES_COL_NAME + " text not null, " +
                SEANCES_COL_DATED + " text, " +
                SEANCES_COL_DATEF + " text, " +
                SEANCES_COL_DATEM + " text, " +
                SEANCES_COL_EVENTID + " integer, " +
                SEANCES_COL_SALLE + " text, " +
                SEANCES_COL_PROF + " text, " +
                SEANCES_COL_GRP + " text " +
                ");";


        public  SeancesHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase database) {
            Log.w(SeancesHelper.class.getName(), "Creating DB ");
            database.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(SeancesHelper.class.getName(),
                    "Upgrading database from version " + oldVersion + " to "
                            + newVersion + ", which will destroy all old data"
            );
            db.execSQL("DROP TABLE IF EXISTS " + SEANCES_TABLE_NAME);
            onCreate(db);
        }
}
