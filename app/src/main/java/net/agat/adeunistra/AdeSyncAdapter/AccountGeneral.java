/*
 * Copyright e.blindauer (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;

/**
 * Created by e.blindauer on 17/07/14.
 */
public class AccountGeneral {

    /**
     * Account type id
     */
    public static final String ACCOUNT_TYPE = "net.agat.adeunistra";

    /**
     * Auth
     * Account name
     */
    public static final String ACCOUNT_NAME = "AdeUnistra";

    /**
     * Auth token types
     */
    public static final String AUTHTOKEN_TYPE = "typeadeunistra";
    public static final String KEY_ADE_RES = "resId";
    public static final String KEY_ADE_PROJ = "projId";

    public static final AdeunistraServer sServerAuthenticate = new AdeunistraServer();


    /**
     * Calendar
     */
    public static final String sCAL_CALENDAR_ACCOUNT_NAME="ADEMobile";
    public static final String sCAL_CALENDAR_NAME="Calendrier ADE Mobile";
    public static final String sCAL_CALENDAR_DISPLAY_NAME="Calendrier ADE Mobile";



}
