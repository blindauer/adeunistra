/*
 * Copyright e.blindauer (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.sCAL_CALENDAR_ACCOUNT_NAME;
import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.sCAL_CALENDAR_DISPLAY_NAME;
import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.sCAL_CALENDAR_NAME;

/**
 * Created by e.blindauer on 06/08/14.
 */
public class AdeCalendar extends ContextWrapper {

    public AdeCalendar(Context context) {
        super(context);
    }

    private String TAG = this.getClass().getSimpleName();

    public long findOrCreateADECalendar() {
        String[] projection =
                new String[]{

                        Calendars._ID,
                        Calendars.NAME,
                        Calendars.ACCOUNT_NAME,
                        Calendars.ACCOUNT_TYPE};
        Cursor calCursor =
                getContentResolver().
                        query(Calendars.CONTENT_URI,
                                projection,
                                Calendars.ACCOUNT_NAME + " = '"+sCAL_CALENDAR_ACCOUNT_NAME +"'",
                                null,
                                Calendars._ID + " ASC");

        if (calCursor.getCount()>0) {
            calCursor.moveToFirst();
            long id = calCursor.getLong(0);
            Log.d(TAG, "id found" + id);
            //verify is visible=1
            updateLocalCalendar(id);
            return id;
        } else {
            return createLocalCalendar();
        }
    }

    private long createLocalCalendar() {

        ContentValues values = new ContentValues();
        values.put(Calendars.ACCOUNT_NAME, sCAL_CALENDAR_ACCOUNT_NAME);
        values.put(Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL);
        values.put(Calendars.NAME, sCAL_CALENDAR_NAME);
        values.put(Calendars.CALENDAR_DISPLAY_NAME, sCAL_CALENDAR_DISPLAY_NAME);
        values.put(Calendars.CALENDAR_COLOR, Color.BLUE);
        values.put(Calendars.CALENDAR_ACCESS_LEVEL, Calendars.CAL_ACCESS_OWNER);
        values.put(Calendars.OWNER_ACCOUNT, "ade@local.local");
        values.put(Calendars.CALENDAR_TIME_ZONE, TimeZone.getDefault().getID());
        values.put(Calendars.SYNC_EVENTS, 1);
        values.put(Calendars.VISIBLE, 1); // Needed for S-planner from Samsung?
        Uri.Builder builder = Calendars.CONTENT_URI.buildUpon();
        builder.appendQueryParameter(Calendars.ACCOUNT_NAME,
                "net.agat.adeunistra");
        builder.appendQueryParameter(Calendars.ACCOUNT_TYPE,
                CalendarContract.ACCOUNT_TYPE_LOCAL);
        builder.appendQueryParameter( CalendarContract.CALLER_IS_SYNCADAPTER,
                "true");
        Uri uri = getContentResolver().insert(builder.build(), values);
        Log.d(TAG, "createcalendar " + uri);

        return ContentUris.parseId(uri);
    }

    private void updateLocalCalendar(long calId) {

        String[] projection =
                new String[]{
                        Calendars._ID,
                        Calendars.NAME,
                        Calendars.ACCOUNT_NAME,
                        Calendars.ACCOUNT_TYPE};
        Cursor calCursor =
                getContentResolver().
                        query(Calendars.CONTENT_URI,
                                projection,
                                "(("+Calendars.ACCOUNT_NAME + " = '"+sCAL_CALENDAR_ACCOUNT_NAME +"') AND " +
                                "("+ Calendars.VISIBLE + "= 0))",
                                null,
                                Calendars._ID + " ASC");
        Log.d(TAG, "updateLocalCalendar : search, found not visible: "+ calCursor.getCount());
        calCursor =
                getContentResolver().
                        query(Calendars.CONTENT_URI,
                                projection,
                                "(("+Calendars.ACCOUNT_NAME + " = '"+sCAL_CALENDAR_ACCOUNT_NAME +"') AND " +
                                        "("+ Calendars.VISIBLE + "= 1))",
                                null,
                                Calendars._ID + " ASC");
        Log.d(TAG, "updateLocalCalendar : search, found visible: "+ calCursor.getCount());
        if (calCursor.getCount()>0) {


            ContentResolver cr = getContentResolver();
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
            values.put(CalendarContract.Calendars.VISIBLE, 1);
            cr.update(
                    ContentUris.withAppendedId(CalendarContract.Calendars.CONTENT_URI, calId),
                    values, null, null);
        }

    }

    private void deleteLocalCalendar(Context ctx) {
        ContentResolver cr = ctx.getContentResolver();
        //TODO WIP
        //Uri calUri = ContentUris.withAppendedId(sCAL_CALENDAR_NAME, sCAL_CALENDAR_NAME);
        //cr.delete(calUri,null,null);
    }

    // supprime tous les evenements apres une certaine date
    public int deleteEvent(Long calId, Long date) {
        getContentResolver().delete(
                Uri.parse("content://com.android.calendar/events"),
                Events.CALENDAR_ID + " = ? and " + Events.DTSTART + " > ? ",
                new String[]{String.valueOf(calId), String.valueOf(date)});
        return 0;
    }
    public int deleteEventByStartEnd(Long calId, Long dated, Long datef) {
        String selection = Events.CALENDAR_ID + " = " + calId + " AND " +Events.DTSTART +"="+dated +
                " AND " + Events.DTEND + " = " + datef ;
        Log.d(TAG, "on va effacer une seance" + selection);
        Uri eventUri = Uri.parse("content://com.android.calendar/events");
        int iNumRowsDeleted = getContentResolver().delete(eventUri, selection, null);
        Log.d(TAG, "Nb effacé:"+iNumRowsDeleted);
        return  0;
    }
    // suprime tous les evenements. interessant quand on change de projet.
    public int deleteAllEvents(Long calId) {
        getContentResolver().delete(
                Uri.parse("content://com.android.calendar/events"),
                Events.CALENDAR_ID + " = ? ",
                new String[]{String.valueOf(calId)});
        return 0;
    }

    public int updateEvent(String dated, String datef, String nom, String salle, String desc){
        Uri eventsUri = Uri.parse("content://com.android.calendar/events");
        String selection =  Events.DTSTART +"='"+dated + "'" +
                " AND " + Events.DTEND +"='"+datef+ "'" ;
        Cursor c = getContentResolver().query(eventsUri, new String []{"_id"}, selection,null, null);
        int id =0;
        while (c.moveToNext()) {
            id = Integer.parseInt(c.getString(0));
        }
        Log.d(TAG, "on va updated une seance" + id);

        Uri eventUri = ContentUris.withAppendedId(eventsUri, id);


        ContentValues event = new ContentValues();
        event.put(Events.DESCRIPTION, desc);
        event.put(Events.EVENT_LOCATION, salle);
        event.put(Events.TITLE, nom);

        int iNumRowsUpdated = getContentResolver().update(eventUri, event, null,
                null);

        Log.i(TAG, "Updated " + iNumRowsUpdated + " calendar entry.");

        return 0;
    }
    public int addEvent(Long calId, Seance seance) {
        long start=0;
        long end=0;
        Date date;

        ContentValues values = new ContentValues();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            date = df.parse(seance.dated);
            start = date.getTime();
            date = df.parse(seance.datef);
            end = date.getTime();

        } catch (Exception e) {
            e.printStackTrace();
        }

        values.put(Events.DTSTART, start);
        values.put(Events.DTEND, end);
        values.put(Events.TITLE, seance.name);
//        values.put(Events.TITLE, seance.name.substring(0, Math.min(seance.name.length(), 15)) + "...");
        values.put(Events.EVENT_LOCATION, seance.salle);
        values.put(Events.DESCRIPTION, "Enseignant: " +seance.prof + "\nGroupe: " + seance.grp);

        values.put(Events.CALENDAR_ID, calId);
        values.put(Events.EVENT_TIMEZONE, "Europe/Paris");

        values.put(Events.ACCESS_LEVEL, Events.ACCESS_PRIVATE);
        values.put(Events.SELF_ATTENDEE_STATUS,
                Events.STATUS_CONFIRMED);
        values.put(Events.ORGANIZER, "ade@local.local");
        values.put(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
        //values.put(Events.SYNC_DATA1, seance.datem); // TODO: réservé sync_adapter, mais on nous refuse l'acces ? il nous manque un marqueur ?

        Uri uri = getContentResolver().insert(Events.CONTENT_URI, values);
        long eventId = new Long(uri.getLastPathSegment());

        return 0;
    }

    public Cursor getEvents() {
        Long calid = findOrCreateADECalendar();
        String selection = Events.CALENDAR_ID + " = " + calid.toString();
        Log.d(TAG, "Recherche de tous les evenements pour calid=" + calid.toString());
        Cursor calCursor =
                getContentResolver().
                        query(Uri.parse("content://com.android.calendar/events"),
                                new String[] { "calendar_id", "title", "description",
                                        "dtstart", "dtend", "eventLocation" }, selection ,
                                null, null);
        return calCursor;
    }

}
