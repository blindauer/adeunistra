/*
 * Copyright e.blindauer (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;

import android.net.Uri;

/**
 * Created by e.blindauer on 16/07/14.
 */
public class SeancesContract {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.agat.seances";
    public static final String CONTENT_TYPE_DIR = "vnd.android.cursor.dir/vnd.agat.seances";

    public static final String AUTHORITY = "net.agat.adeunistra.provider";
    // content://<authority>/<path to type>
    public static final Uri CONTENT_URI = Uri.parse("content://"+AUTHORITY+"/seances");

    public static final String SEANCE_ID = "_id";
    public static final String SEANCE_NAME = "name";
    public static final String SEANCE_DATEF = "dateF";
    public static final String SEANCE_DATED = "dateD";
    public static final String SEANCES_EVENTID = "eventid";
    public static final String SEANCE_SALLE = "salle";
    public static final String SEANCE_PROF = "prof";


}
