/*
 * Copyright e.blindauer (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class AdeSyncService extends Service {

    private static final Object sSyncAdeAdapterLock = new Object();
    public static AdeSyncAdapter sSyncAdeAdapter = null;

    public AdeSyncService() {
    }
    public void onCreate(){
        synchronized (sSyncAdeAdapterLock) {
               if (sSyncAdeAdapter == null )
                   sSyncAdeAdapter = new AdeSyncAdapter(getApplicationContext(),true);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sSyncAdeAdapter.getSyncAdapterBinder();
    }
}
