/*
 * Copyright e.blindauer (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SyncStatusObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.agat.adeunistra.BuildConfig;
import net.agat.adeunistra.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.KEY_ADE_PROJ;
import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.KEY_ADE_RES;
import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.sServerAuthenticate;
import static net.agat.adeunistra.AdeSyncAdapter.AdeSyncService.sSyncAdeAdapter;

public class actDisplayData extends Activity {

    private String TAG = this.getClass().getSimpleName();
    private AccountManager mAccountManager;
    private String authToken = null;
    private Account mConnectedAccount;

    private AdeCalendar adeCalendar = new AdeCalendar(this);

    private static ArrayList<Integer> resourceId=null;
    private static Integer projectId=-1;
    public final static String KEY_ERROR_MESSAGE = "ERR_MSG";


    SyncStatusObserver syncObserver = new SyncStatusObserver() {
        @Override
        public void onStatusChanged(final int which) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshSyncStatus();
                }
            });
        }
    };

    Object handleSyncObserver;
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
        handleSyncObserver = ContentResolver.addStatusChangeListener(ContentResolver.SYNC_OBSERVER_TYPE_ACTIVE |
                ContentResolver.SYNC_OBSERVER_TYPE_PENDING, syncObserver);
        refreshSyncStatus();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handleSyncObserver != null)
            ContentResolver.removeStatusChangeListener(handleSyncObserver);
        super.onStop();
        Log.d(TAG,"onPause");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_display_data);


        /* cuted */

        /**
         *       Account stuff
         */

        ((Button)findViewById(R.id.btnOpenCalendar)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mConnectedAccount == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(actDisplayData.this);
                    builder.setMessage("Veuillez passez par le menu  pour vous connecter. (Il faut avoir personnalisé son emploi du temps dans l'ENT).\nAprès connexion vous retrouverez ADE dans le calendrier local");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("content://com.android.calendar/time"));
                    startActivity(i);
                }

            }
        });
/*
        ((Button)findViewById(R.id.btnCheck)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                doCheck();
            }

        });
*/
        ((CheckBox)findViewById(R.id.isSyncable)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mConnectedAccount == null) {
                    return;
                }

                // Setting the syncable state of the sync adapter
                String authority = SeancesContract.AUTHORITY;
                ContentResolver.setIsSyncable(mConnectedAccount, authority, isChecked ? 1 : 0);
            }
        });

        ((CheckBox)findViewById(R.id.autosync)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mConnectedAccount == null) {
                    Toast.makeText(actDisplayData.this, "Pas de Token", Toast.LENGTH_SHORT).show();
                    return;
                }
                // Setting the autosync state of the sync adapter
                String authority = SeancesContract.AUTHORITY;
                ContentResolver.setSyncAutomatically(mConnectedAccount,authority, isChecked);
            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


    }


    protected void verifyPermissions() {
        /*
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(thisActivity,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);


        }*/
    }


    @Override
    protected void onStart() {
        super.onStart();
        // show dialog here
        verifyPermissions();
        mAccountManager = AccountManager.get(this);
        Account[] allAccounts = mAccountManager.getAccountsByType(AccountGeneral.ACCOUNT_TYPE);
        if (allAccounts.length!=0) {
            mConnectedAccount = allAccounts[0];
            try {

                projectId = Integer.parseInt(mAccountManager.getUserData(mConnectedAccount,KEY_ADE_PROJ));
                String[] resourceString = (mAccountManager.getUserData(mConnectedAccount,KEY_ADE_RES)).split(",");
                resourceId = new ArrayList<Integer>();
                int i=0;
                for (String str : resourceString)
                    resourceId.add(i++,Integer.parseInt(str));
                if ((projectId!=-1) && (resourceId!=null)) authToken = "adeunistra";
            } catch (Exception e) {
                // on a pas reussi a avoir les ressources ou le token, donc on va rien faire
                AlertDialog.Builder builder = new AlertDialog.Builder(actDisplayData.this);
                builder.setMessage("Aucune préférence n'a été trouvée sur l'ENT. Rendez vous dans l'ENT et effectuez une personnalisation de votre emploi du temps");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();

            }
            Log.d(TAG,"oncreate, projectId:" + projectId + " resId: "+ resourceId + " Token: "+ authToken);

            ContentResolver.setIsSyncable(mConnectedAccount, SeancesContract.AUTHORITY, 1);
            ContentResolver.setSyncAutomatically(mConnectedAccount, SeancesContract.AUTHORITY, true);
            ContentResolver.addPeriodicSync(mConnectedAccount, SeancesContract.AUTHORITY,new Bundle(), 3600L);
            initButtonsAfterConnect();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                    android.R.layout.simple_list_item_1, (List)readFromContentProviderSorted());

            ((ListView) findViewById(R.id.listView)).setAdapter(adapter);
            ((ListView) findViewById(R.id.listView)).setTextFilterEnabled(true);


        } else {
            // Pas de compte de trouvé


            AlertDialog.Builder builder = new AlertDialog.Builder(actDisplayData.this);
            builder.setMessage("Bienvenue. Veuillez passez par le menu  pour vous connecter. (Il faut avoir personnalisé son emploi du temps dans l'ENT).\nAprès connexion vous retrouverez ADE dans le calendrier local");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show();

            initButtonsAfterConnect();

        }

    }

    private void showOnDialog(String title, List<Seance> Seances) {
        AlertDialog.Builder builder = new AlertDialog.Builder(actDisplayData.this);
        builder.setTitle(title);
        builder.setAdapter(new ArrayAdapter<Seance>(actDisplayData.this, android.R.layout.simple_list_item_1, Seances),null);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

    private void refreshSyncStatus() {
        String status;
        if (mConnectedAccount==null) {
            status = "Status: inactif";
        } else {
            if (ContentResolver.isSyncActive(mConnectedAccount, SeancesContract.AUTHORITY))
                status = "Status: Syncing..";
            else if (ContentResolver.isSyncPending(mConnectedAccount, SeancesContract.AUTHORITY))
                status = "Status: Pending..";
            else
                status = "Status: Idle";
        }
        ((TextView) findViewById(R.id.status)).setText(status);
        Log.d(TAG, "refreshSyncStatus> " + status);
    }

    private void initButtonsAfterConnect() {
        String authority = SeancesContract.AUTHORITY;
        Log.d(TAG, "initButtonsAfterConnect");
        // Get the syncadapter settings and init the checkboxes accordingly
        int isSyncable = 0;

        boolean autSync = false;


        ((CheckBox)findViewById(R.id.isSyncable)).setChecked(isSyncable > 0);
        ((CheckBox)findViewById(R.id.autosync)).setChecked(autSync);

        findViewById(R.id.isSyncable).setEnabled(true);
        findViewById(R.id.autosync).setEnabled(true);
        findViewById(R.id.status).setEnabled(true);

        if (mConnectedAccount!=null) {
            isSyncable = ContentResolver.getIsSyncable(mConnectedAccount, authority);
            autSync = ContentResolver.getSyncAutomatically(mConnectedAccount, authority);
            ((CheckBox)findViewById(R.id.isSyncable)).setChecked(isSyncable > 0);
            ((CheckBox)findViewById(R.id.autosync)).setChecked(autSync);
            ((EditText) findViewById(R.id.txtProject)).setText(mAccountManager.getUserData(mConnectedAccount, KEY_ADE_PROJ));
            ((EditText) findViewById(R.id.txtRessources)).setText(mAccountManager.getUserData(mConnectedAccount, KEY_ADE_RES));
            refreshSyncStatus();
        }
    }

    private List<Seance> readFromContentProvider() {
        Cursor curSeances = getContentResolver().query(SeancesContract.CONTENT_URI, null, null, null, null);

        ArrayList<Seance> seances = new ArrayList<Seance>();

        if (curSeances != null) {
            while (curSeances.moveToNext())
                seances.add(Seance.fromCursor(curSeances));
            curSeances.close();
        }
        return seances;
    }


    /* limité a 5*/
    private List<Seance> readFromContentProviderSorted() {
        String selection = "date(dateD) > date('now') ";
        String sortOrder = "date(dateD) ASC";
        int i=0;
        Cursor curSeances = getContentResolver().query(SeancesContract.CONTENT_URI, null, selection, null, sortOrder);

        ArrayList<Seance> seances = new ArrayList<Seance>();

        if (curSeances != null) {
            while ((curSeances.moveToNext()) && (i<6)) {
                seances.add(Seance.fromCursor(curSeances));
                i++;
            }
            curSeances.close();
        }
        return seances;
    }
    /**
     * Get an auth token for the account.
     * If not exist - add it and then return its auth token.
     * If one exist - return its auth token.
     * If more than one exists - show a picker and return the select account's auth token.
     * @param accountType
     * @param authTokenType
     */
    private void getTokenForAccountCreateIfNeeded(String accountType, String authTokenType) {
        final AccountManagerFuture<Bundle> future = mAccountManager.getAuthTokenByFeatures(accountType, authTokenType, null, this, null, null,
                new AccountManagerCallback<Bundle>() {
                    @Override
                    public void run(AccountManagerFuture<Bundle> future) {
                        Bundle bnd = null;
                        try {
                            bnd = future.getResult();
                            authToken = bnd.getString(AccountManager.KEY_AUTHTOKEN);
                            if (authToken != null) {
                                String accountName = bnd.getString(AccountManager.KEY_ACCOUNT_NAME);
                                mConnectedAccount = new Account(accountName, AccountGeneral.ACCOUNT_TYPE);
                                ContentResolver.setIsSyncable(mConnectedAccount, SeancesContract.AUTHORITY, 1);
                                ContentResolver.setSyncAutomatically(mConnectedAccount, SeancesContract.AUTHORITY, true);
                                ContentResolver.addPeriodicSync(mConnectedAccount, SeancesContract.AUTHORITY,new Bundle(), 3600L);
                                initButtonsAfterConnect();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            showMessage(e.getMessage());
                        }
                    }
                }
                , null);
    }

    private void getRessourcesFromADEWO() {
        new AsyncTask<String, Void, Intent>() {

            @Override
            protected Intent doInBackground(String... params) {
                Log.d(TAG, "> Started gettings ressources");

                Bundle data = new Bundle();
                try {
                    AccountManager.get(getBaseContext()).getPassword(mConnectedAccount);

                    AdeunistraServer.AdeunistraResponse resp = sServerAuthenticate.getRessourcesADE(
                            mConnectedAccount.name,
                            AccountManager.get(getBaseContext()).getPassword(mConnectedAccount));
                    mAccountManager.setUserData(mConnectedAccount, KEY_ADE_RES, TextUtils.join(",", resp.resId));
                    mAccountManager.setUserData(mConnectedAccount, KEY_ADE_PROJ, ((Integer) resp.projectId).toString() );
                } catch (Exception e) {
                    data.putString(KEY_ERROR_MESSAGE,e.toString());
                }
                final Intent res = new Intent();
                res.putExtras(data);
                return res;
            }

            @Override
            protected void onPostExecute(Intent intent) {
                if (intent.hasExtra(KEY_ERROR_MESSAGE)) {
                    Toast.makeText(getBaseContext(), intent.getStringExtra(KEY_ERROR_MESSAGE), Toast.LENGTH_SHORT).show();
                } else {
                    if (mConnectedAccount!=null) {
                        String ressources = mAccountManager.getUserData(mConnectedAccount, KEY_ADE_RES);
                        ((EditText) findViewById(R.id.txtProject)).setText(mAccountManager.getUserData(mConnectedAccount, KEY_ADE_PROJ));
                        ((EditText) findViewById(R.id.txtRessources)).setText(ressources);
                        Toast.makeText(getBaseContext(),
                                "Ressources mises à jour: " + ressources , Toast.LENGTH_LONG).show();
                    }
               }
            }
        }.execute();
    }

    private void showMessage(final String msg) {
        if (msg == null || msg.trim().equals(""))
            return;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_connect:
                if (mConnectedAccount == null ) {
                    getTokenForAccountCreateIfNeeded(AccountGeneral.ACCOUNT_TYPE, AccountGeneral.AUTHTOKEN_TYPE);
                } else {
                    Toast.makeText(actDisplayData.this,
                            "Vous avez déjà un compte AdeUnistra sur ce téléphone. Pour le retirer, allez dans \"Configuration\" puis \"Comptes\".", Toast.LENGTH_LONG).show();
                }
                return true;


            case R.id.action_clearseances:
                int numDeleted = getContentResolver().delete(SeancesContract.CONTENT_URI, null, null);
                if (sSyncAdeAdapter!= null)
                    sSyncAdeAdapter.resetLastAccess();
                Toast.makeText(actDisplayData.this, "Séances supprimées : " + numDeleted, Toast.LENGTH_SHORT).show();
                return true;



            case R.id.action_seelocalseances:
                List<Seance> list = readFromContentProvider();
                AlertDialog.Builder builder = new AlertDialog.Builder(actDisplayData.this);
                builder.setAdapter(new ArrayAdapter<Seance>(actDisplayData.this, android.R.layout.simple_list_item_1, list),null);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
                return  true;

            case R.id.action_sync:
                Log.d(TAG,"sync");
                if (mConnectedAccount == null) {
                    Toast.makeText(actDisplayData.this, "Vous n'êtes pas connecté!", Toast.LENGTH_SHORT).show();
                    return false;
                }
                Log.d(TAG,"if sSyncAdeAdapter?");
                if (sSyncAdeAdapter!= null)
                    sSyncAdeAdapter.resetLastAccess();
                Bundle bundle = new Bundle();
                bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true); // Performing a sync no matter if it's off
                bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true); // Performing a sync no matter if it's off
                if (ContentResolver.isSyncPending(mConnectedAccount, SeancesContract.AUTHORITY)  ||
                        ContentResolver.isSyncActive(mConnectedAccount, SeancesContract.AUTHORITY)) {
                    Log.d("ContentResolver", "SyncPending, canceling");
                    ContentResolver.cancelSync(mConnectedAccount, SeancesContract.AUTHORITY);
                }
                Log.d(TAG,"requestSync");
                ContentResolver.requestSync(mConnectedAccount, SeancesContract.AUTHORITY, bundle);
                return true;
            case R.id.action_clearcalendar:
                if (mConnectedAccount == null) {
                    Toast.makeText(actDisplayData.this, "Vous n'êtes pas connecté!", Toast.LENGTH_SHORT).show();
                    return false;
                }
                Long calId = adeCalendar.findOrCreateADECalendar();
                adeCalendar.deleteAllEvents(calId);
                return true;

            case R.id.action_resyncressources:
                if (mConnectedAccount == null) {
                    Toast.makeText(actDisplayData.this, "Vous n'êtes pas connecté!", Toast.LENGTH_SHORT).show();
                    return false;
                }
                getRessourcesFromADEWO();
                return true;
            case R.id.action_about:
                PackageManager manager = this.getPackageManager();
                String version="unknown";
                try {
                    PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
                    version = info.versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                AlertDialog.Builder builder2 = new AlertDialog.Builder(actDisplayData.this);
                builder2.setMessage("Ade Mobile Unistra v"+version+
                        " (c) 2014-2017\nUne réalisation de l'IUT R.Schuman\nToutes les infos sur \nhttp://ade.agat.net/");
                builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }





}
