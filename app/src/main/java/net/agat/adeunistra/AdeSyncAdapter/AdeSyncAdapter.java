/*
 * Copyright e.blindauer (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.agat.adeunistra.AdeSyncAdapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import net.agat.adeunistra.BuildConfig;

import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.KEY_ADE_PROJ;
import static net.agat.adeunistra.AdeSyncAdapter.AccountGeneral.KEY_ADE_RES;


/**
 * Created by e.blindauer on 16/07/14.
 */
public class AdeSyncAdapter extends AbstractThreadedSyncAdapter {

    private final String TAG = this.getClass().getSimpleName();
    private final AccountManager mAccountManager;
    private static ArrayList<Integer> resourceId = null;
    private static Integer projectId = -1;
    private AdeCalendar adeCalendar = null;
    private static Long lastUpdate = 0L;


    public AdeSyncAdapter(Context c, boolean initialized) {
        super(c, initialized);
        mAccountManager = AccountManager.get(c);
        adeCalendar = new AdeCalendar(c);
    }

    private String getDate(long milliSeconds) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    //used to force an update from server if we clear the local cache.
    public void resetLastAccess() {
        Log.d(TAG, "> resetLastAccess ");
        lastUpdate = 0L;
    }

    public void onPerformSync(Account account, Bundle extras, final String authority,
                              ContentProviderClient provider, SyncResult syncResult) {

        Log.d(TAG, "> onPerformSync for account[" + account.name + "]. ");
        String authToken = null;
        ArrayList<Seance> remoteSeances = null;
        ArrayList<Seance> localSeances = new ArrayList();
        Long currentUpdate = new Date().getTime();
        int delay;

        if (BuildConfig.DEBUG) {
            delay = 1000 * 30;
        } else {
            delay = 1000 * 3600;
        }
        if (currentUpdate - lastUpdate < delay) {
            // Wait one hour (3600x1000 ms) between update. the autosync makes us wakeup
            // every minute if there is network, but we don't need to load the server.
            return;
        }
        Log.d(TAG, "> onPerformSync timeout");
        //On devrait avoir un compte avec les infos, car on n'active la sync que dans ce cas. Bretelles, ceintures ...
        try {
            Account[] allAccounts = mAccountManager.getAccountsByType(AccountGeneral.ACCOUNT_TYPE);
            if (allAccounts.length != 0) {
                projectId = Integer.parseInt(mAccountManager.getUserData(allAccounts[0], KEY_ADE_PROJ));
                if (projectId != BuildConfig.ADEPROJECT) {
                    Log.d(TAG, "onPerformSync> , projectId:" + projectId + " BuildConfig.ADEPROJECT: " + BuildConfig.ADEPROJECT + ": on est pas bon.");
                    NotificationHelper.signalSyncErrors(this.getContext(), "Adeunistra: anciennes préfs. 2015",
                            "Vérifiez la personnalisation, rechargez la config ENT!");
                }

                String[] resourceString = (mAccountManager.getUserData(allAccounts[0], KEY_ADE_RES)).split(",");
                resourceId = new ArrayList<Integer>();
                int i = 0;
                for (String str : resourceString)
                    resourceId.add(i++, Integer.parseInt(str));
                if ((projectId != -1) && (resourceId != null)) {
                    authToken = "adeunistra";
                } else {
                    throw new Exception("Pas de parametre userdata trouvé dans le compte");
                }
            } else {
                // on n'a pas de compte  ca ne devrait pas arriver
                throw new Exception("No Account found");
            }

            AdeunistraServerAccessor adeservice = new AdeunistraServerAccessor();
            remoteSeances = adeservice.getSeances(projectId, resourceId);

            ArrayList<Integer> eventid_present = new ArrayList<Integer>();

            provider.delete(SeancesContract.CONTENT_URI, null, null);
            for (Seance localSeance : remoteSeances) {
                if (!eventid_present.contains(localSeance.eventid)) {
                    provider.insert(SeancesContract.CONTENT_URI, localSeance.getContentValues());
                    eventid_present.add(localSeance.eventid);
                    localSeances.add(localSeance);
                }
            }

            if ((remoteSeances.size() < 3) && (localSeances.size() > 7)) {
                //moins de 3 seances en remote ? y a peut etre une erreur coté serveur, on ne va pas synchroniser
                return;
            }

            // A partir de maintenant, il faut utiliser uniquement localSeance qui contient des données uniques.

            long calId = adeCalendar.findOrCreateADECalendar();

            int countAdded = 0, countModified = 0, countSame = 0, countRemoved = 0;
            Date date = new Date();
            Long now = date.getTime();
            if (false) {
                // La méthode bourrin, mais ca a l'air de poser des pb sur les tel samsung avec Splanner
                adeCalendar.deleteEvent(calId, now);

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                for (Seance l : remoteSeances) {
                    try {
                        date = df.parse(l.dated);
                        Long start = date.getTime();
                        if (start > now) {
                            adeCalendar.addEvent(calId, l);
                            countAdded++;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                /*
                   Preparer tableau // a la DB, a false
                   pour chaque event du calendrier:
                     - extraitre l'heure de debut et fin
                     - Parcourir la DB (date)
                       * comparer les dates
                       * Si trouvé test sur les ressources:
                          * meme ressources -> tablea[i]=true
                          * pas meme ressources
                            * -> on update
                            * -> on met à true
                       * pas trouvé -> delete.
                   -> pour toutes les entrées restantes a false, ajouter dans le cal.
                 */
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Boolean found[] = new Boolean[localSeances.size()];
                Arrays.fill(found, false);

                Cursor eventCursor = adeCalendar.getEvents();

                Log.d(TAG, "eventCursor count=" + eventCursor.getCount());
                //1 = name   //2 = prof/groupe // 3=DTSTART 4=DTEND  //5 = salle

                if (eventCursor.getCount() > 0) {
                    if (eventCursor.moveToFirst()) {
                        do {
                            String currentDTSTART = getDate(Long.parseLong(eventCursor.getString(3)));
                            String currentDTEND = getDate(Long.parseLong(eventCursor.getString(4)));
                            Log.d(TAG, "Recherche de seance du " + currentDTSTART + currentDTEND +
                                    "Calendrier: " + eventCursor.getString(1) +
                                    eventCursor.getString(2) + eventCursor.getString(5));
                            int isfound = -1;
                            // boucle for (Seance l : remoteSeances)
                            for (int j = 0; j < localSeances.size(); j++) {
                                if ((currentDTSTART.equals(localSeances.get(j).dated)) &&
                                        (currentDTEND.equals(localSeances.get(j).datef)) &&
                                        found[j] == false
                                        ) {
                                    Log.d(TAG, "identique: " + currentDTSTART +
                                            localSeances.get(j).dated + currentDTEND +
                                            localSeances.get(j).datef);
                                    isfound = j;
                                    break;
                                }
                            }
                            if (isfound != -1) {
                                // on a trouvé un event qui correspond
                                String ensgrp = "Enseignant: " + localSeances.get(isfound).prof +
                                        "\nGroupe: " + localSeances.get(isfound).grp;
                                if (localSeances.get(isfound).name.equals(eventCursor.getString(1)) &&
                                        localSeances.get(isfound).salle.equals(eventCursor.getString(5)) &&
                                        ensgrp.equals(eventCursor.getString(2))) {
                                    // meme seance
                                    Log.d(TAG, "Seance identique");
                                    found[isfound] = true;
                                    countSame++;
                                } else {
                                    // description a modifier
                                    Log.d(TAG, "Seance modif" + localSeances.get(isfound).name +
                                            localSeances.get(isfound).salle + ensgrp);
                                    found[isfound] = true;
                                    // On passe poru dated datef lles valeurs dans le cursor car elles sont deja en MS et sont egales aux chaines
                                    adeCalendar.updateEvent(eventCursor.getString(3), eventCursor.getString(4),
                                            localSeances.get(isfound).name,
                                            localSeances.get(isfound).salle,
                                            ensgrp
                                    );
                                    countModified++;
                                    //TODO
                                }
                            } else {
                                // Pas d'event de trouvé a la meme date, on delete la seance
                                Log.d(TAG, "Séance inexistante, on supprime");
                                adeCalendar.deleteEventByStartEnd(calId,
                                        Long.parseLong(eventCursor.getString(3)),
                                        Long.parseLong(eventCursor.getString(4)));
                                countRemoved++;
                            }
                        } while (eventCursor.moveToNext());
                    }
                }
                // maintenant on regarde ce qui reste a true
                for (int j = 0; j < found.length; j++) {
                    if (found[j] == false) {
                        Log.d(TAG, "Des nouvelles seances a ajouter");
                        Log.d(TAG, "XXX:" + localSeances.get(j).dated
                                + " " + localSeances.get(j).datef
                                + " " + localSeances.get(j).datem);
                        adeCalendar.addEvent(calId, localSeances.get(j));
                        countAdded++;
                    }
                }
            }

            Log.d(TAG, "Seances ajoutées:" + countAdded + " Seances supprimés: " + countRemoved +
                    " Seances modifiées: " + countModified + "Seances identiques: " + countSame);
            lastUpdate = currentUpdate;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
